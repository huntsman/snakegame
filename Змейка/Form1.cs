﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Змейка
{
    public partial class Form1 : Form
    {
        //List<SnakeElement> ElementList = new List<SnakeElement>();
        Snake GSnake;
        bool GameisStarted = false;
        bool GameisPlay = false;
        int itemp = 0;
        bool die;




        //struct SnakeElement
        //{
        //    public PictureBox Element;
        //    public int X;
        //    public int Y;
        //}

        public Form1()
        {
            InitializeComponent();
            Area.CreateArea(12, 12);

            //PictureBox Pb = new PictureBox();
            //Pb.Image = Image.FromFile("./Data/Pole.png");
            //Pb.Location = new Point();
            //Pb.Size = new Size(328, 374);
            //Pb.TabStop = false;
            //Pb.Name = "EPole";
            //Pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            //Controls.Add(Pb);
        }

        void delete()
        {
            Controls.Remove(GSnake.SElement[0]);
            if (GSnake.SElement.Count == 1)
            {
                GameisStarted = false;
                SnakeStop();
                BeginB.Enabled = false;
                die = true;
                ShowScore();
                return;
            }
            GSnake.Delete(0);
           // if (GSnake.SElement.Count == 0)

        }

        void ContAdd()
        {
            Controls.Add(Generator.GObjects[Generator.GObjects.Count -1]);
        }

        void NextLevel()
        {
            StaticData.Level += 1;
            LevelText.Text = "Уровень " + StaticData.Level;
            LevelText.Visible = true;
            BeginB.Visible = true;

        }



        //public string readTextFile(string sFileName)
        //{
        //    string s;

        //    using (System.IO.StreamReader sr = new
        //            System.IO.StreamReader(sFileName))
        //    {
        //        s = sr.ReadToEnd();
        //    }

        //    return s;
        //}

        void BuildLevel()
        {
            //string s = readTextFile("./Data/Levels/" + Level + ".lvl");
            string[] s = File.ReadAllLines("./Data/Levels/" + StaticData.Level + ".lvl");
            int index = 0;
            GSnake = new Snake();
            foreach (string str in s)
            {
                foreach (char c in str)
                {
                    if (c == 'X')
                    {
                        Element Pb = new Element();
                        Pb.Image = Image.FromFile("./Data/Стена.png");
                        Pb.Position = Area.Element(index);
                        Pb.Location = Area.Element(Pb.Position.X, Pb.Position.Y);
                        Pb.Size = new Size(26, 26);
                        Pb.Type = 2; // Стена
                        Pb.BackColor = System.Drawing.Color.Transparent;
                        Pb.TabStop = false;
                        Pb.Name = "E" + Generator.GObjects.Count;
                        Generator.GObjects.Add(Pb);
                    }
                    if (c == 'Y')
                    {
                        GSnake.Add(Area.Element(index).X, Area.Element(index).Y);
                    }
                    index += 1;
                }
            }
        }

        void EndLevel()
        {
            GameisStarted = false;
            Controls.Remove(GSnake.SElement[0]);
            GSnake.Delete(0);
            if (GSnake.SElement.Count == 0)
                ShowScore();
                
        }

        void load()
        {
            GSnake.Click += new DelElement(delete);
            GSnake.On += new End(EndLevel);
            Generator.Click += new AddElement(ContAdd);
            Generator.GenerateFood(GSnake.PointList());
            Generator.GenerateWall();

            for (int i = 0; GSnake.SElement.Count > i; i++)
            {
                Controls.Add(GSnake.SElement[i]);
            }
            for (int i = 0; Generator.GObjects.Count > i; i++)
            {
                Controls.Add(Generator.GObjects[i]);
            }
            GameisStarted = true;
            itemp = GSnake.SElement.Count;

            GameisPlay = true;

        }

        void AllClear()
        {
            if (!die)
            {
                if (GameisPlay)
                {
                    SnakeStop();
                    for (int i = 0; GSnake.SElement.Count > i; i++)
                    {
                        Controls.Remove(GSnake.SElement[i]);
                    }
                    GSnake.SElement.Clear();
                    for (int i = 0; Generator.GObjects.Count > i; i++)
                    {
                        Controls.Remove(Generator.GObjects[i]);
                    }
                    Generator.GObjects.Clear();
                    GameisStarted = false;
                    itemp = 0;
                    Generator.clearbool();
                }
            }
            else
                for (int i = 0; GSnake.SElement.Count > i; i++)
                {
                    Controls.Remove(GSnake.SElement[i]);
                }
            for (int i = 0; Generator.GObjects.Count > i; i++)
            {
                Controls.Remove(Generator.GObjects[i]);
            }
        }

        void DelElement()
        {
        }

        private void новаяИграToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticData.Level = 0;
            StaticData.Eaten = 0;
            StaticData.LevelBonus = 0;
            StaticData.Score = 0;
            label1.Visible = false;
            lScore.Visible = false;
            lBonus.Visible = false;
            lAllscore.Visible = false;
            die = false;
            BeginB.Text = "Начать";
            BeginB.Enabled = true;
            AllClear();
            NextLevel();
        }

        void CreateSnake()
        {
           // Contr
        }

        void CreateArea()
        {

        }

        void SnakeRun()
        {
            timer1.Enabled = true;
        }
        void SnakeStop()
        {
            timer1.Enabled = false;
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            if (StaticData.Eaten == 6 + StaticData.Level)
            {
                Generator.visfood = true;
                Generator.GeneratePortal(GSnake.PointList());

            }

            if (itemp < GSnake.SElement.Count)
                Controls.Add(GSnake.SElement[GSnake.SElement.Count -1]);
            itemp = GSnake.SElement.Count;
            GSnake.Run();

        }

        void ShowScore()
        {
            AllClear();
            lScore.Text = "Счёт: " + StaticData.Score;
            lBonus.Text = "Бонус: " + StaticData.LevelBonus;
            lAllscore.Text = "Всего: " + (StaticData.Score + StaticData.LevelBonus);
            label1.Visible = true;
            lScore.Visible = true;
            lBonus.Visible = true;
            lAllscore.Visible = true;
            BeginB.Text = "Продолжить";
            NextLevel();
            StaticData.Eaten = 0;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (GameisStarted)
            {

                if (e.KeyCode == Keys.W)
                {
                    if (GSnake.direction != 1)
                        GSnake.direction = 0;
                }
                else if (e.KeyCode == Keys.S)
                {
                    if (GSnake.direction != 0)
                        GSnake.direction = 1;
                }
                else if (e.KeyCode == Keys.A)
                {
                    if (GSnake.direction != 3)
                        GSnake.direction = 2;
                }
                else if (e.KeyCode == Keys.D)
                {
                    if (GSnake.direction != 2)
                        GSnake.direction = 3;
                }

                if (timer1.Enabled == false)
                    SnakeRun();

            }
        }

        private void BeginB_Click(object sender, EventArgs e)
        {
            LevelText.Text = "";
            label1.Visible = false;
            lScore.Visible = false;
            lBonus.Visible = false;
            lAllscore.Visible = false;
            LevelText.Visible = false;
            BeginB.Visible = false;
            this.Focus();
            //BeginB.
            BuildLevel();
            load();
        }
    }
}
