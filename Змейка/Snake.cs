﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Змейка
{

    delegate void DelElement();
    delegate void End();

    class Snake
    {
        public byte direction = 4;
        //0 - вверх, 1 - вниз, 2 - налево, 3 - направо , 4 - покоиться
        public List<Element> SElement;
        Image IEl;


        public DelElement Click;
        public End On;


        public Snake()
        {
            SElement = new List<Element>();
            IEl = Image.FromFile("./Data/Шар.png");
            //Add();
            //Add();
            //Add();
            //Add();
        }

        public Point[] PointList()
        {
            Point[] Pl = new Point[SElement.Count];
            for (int i = 0; Pl.Length > i; i++)
            {
                Pl[i] = SElement[i].Position;
            }

            return Pl;
        }

        public void Add(int str, int st)
        {
            Element Pb = new Element();
            Pb.Image = IEl;
            Pb.Position = new Point(str, st);
            Pb.Location = Area.Element(str,st);
            Pb.Size = new Size(26, 26);
            Pb.BackColor = System.Drawing.Color.Transparent;
            Pb.TabStop = false;
            Pb.Name = "E" + SElement.Count;
            SElement.Add(Pb);

        }

        public void Add()
        {
            Element Pb = new Element();
            Pb.Image = IEl;
            Pb.Position = new Point(SElement[SElement.Count - 1].Position.X, SElement[SElement.Count - 1].Position.Y + 1);
            Pb.Location = Area.Element(Pb.Position.X, Pb.Position.Y);
            Pb.Size = new Size(26, 26);
            Pb.BackColor = System.Drawing.Color.Transparent;
            Pb.TabStop = false;
            Pb.Name = "E" + SElement.Count;
            SElement.Add(Pb);

        }

        void Plus1(int Pstr, int Pst)
        {
            Point LastPoint = new Point();
            for (int i = 0; SElement.Count > i; i++)
            {
                if (LastPoint.IsEmpty)
                {
                    LastPoint = SElement[i].Position;
                    SElement[i].ChangePosition(Pstr, Pst);
                }
                else
                {
                    Pstr = SElement[i].Position.X;
                    Pst = SElement[i].Position.Y;
                    SElement[i].ClonePosition(LastPoint.X, LastPoint.Y);
                    LastPoint = new Point(Pstr, Pst);

                }

            }

        }

        public void Run()
        {


            switch (direction)
            {
                case 0: //вверх
                    {
                        Plus1(-1,0);
                        break;
                    }
                case 1: //вниз
                    {
                        Plus1(1, 0);
                        break;
                    }
                case 2: //налево
                    {
                        Plus1(0, -1);
                        break;
                    }
                case 3: //направо
                    {
                        Plus1(0, 1);
                        break;
                    }
            }


                for (int i = 0; SElement.Count > i; i++)
                {

                    for (int j = 0; Generator.GObjects.Count > j; j++)
                    {
                        if (SElement[i].Position == Generator.GObjects[j].Position)
                            if (Generator.GObjects[j].Type == 1)
                            {
                                StaticData.Score += 2 * StaticData.Level;
                                StaticData.Eaten += 1;
                                Add(SElement[SElement.Count - 1].Position.X, SElement[SElement.Count - 1].Position.Y);
                                Generator.GenerateFood(PointList());
                            }
                        if (SElement[i].Position == Generator.GObjects[j].Position)
                            if (Generator.GObjects[j].Type == 2)
                                if (Click != null)
                                    Click();

                        if (SElement[i].Position == Generator.GObjects[j].Position)
                            if (Generator.GObjects[j].Type == 3)
                                if (On != null)
                                    On();
                    }
                    if (i > 0)
                        if (SElement[0].Position == SElement[i].Position)
                            if (Click != null)
                                Click();
                }

        }


        public void Delete(int index)
        {
            SElement.RemoveAt(index);
        }
    }
}
