﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace Змейка
{
    delegate void AddElement();

    static class Generator
    {
        public static List<Element> GObjects = new List<Element>();
        static bool food;
        public static bool visfood;
        public static AddElement Click;
        static bool portaladded;

        public static void clearbool()
        {
            visfood = false;
            food = false;
            portaladded = false;
        }

        static Point RandomPoint()
        {
            Random rnd = new Random();
            return new Point(rnd.Next(1,12),rnd.Next(1,12));
        }

        public static void GenerateFood(Point[] list)
        {
            if (!visfood)
            {
                int index = 0;
                if (food == true)
                    for (int i = 0; GObjects.Count > i; i++)
                    {
                        if (GObjects[i].Type == 1)
                        {

                            index = i;
                            break;
                        }
                    }
                if (food == true)
                    GObjects[index].Visible = false;
                Point pt = RandomPoint();

                for (int i = 0; list.Length > i; i++)
                {
                    if (pt == list[i])
                    {
                        pt = RandomPoint();
                        i = 0;
                    }
                    if (i == list.Length - 1)
                        for (int j = 0; GObjects.Count > j; j++)
                        {
                            if (pt == GObjects[j].Position)
                            {
                                pt = RandomPoint();
                                i = 0;
                                break;
                            }
                        }

                }

                if (food == false)
                {
                    Element Pb = new Element();
                    Pb.Image = Image.FromFile("./Data/Еда.png");
                    Pb.Position = pt;
                    Pb.Location = Area.Element(Pb.Position.X, Pb.Position.Y);
                    Pb.Size = new Size(26, 26);
                    Pb.Type = 1; // Еда
                    Pb.BackColor = System.Drawing.Color.Transparent;
                    Pb.TabStop = false;
                    Pb.Name = "E" + GObjects.Count;
                    GObjects.Add(Pb);
                    food = true;
                }
                else
                {
                    GObjects[index].Position = RandomPoint();
                    GObjects[index].Location = Area.Element(GObjects[index].Position.X, GObjects[index].Position.Y);
                    GObjects[index].Visible = true;
                }
            }
            else
                for (int i = 0; GObjects.Count > i; i++)
                {
                    if (GObjects[i].Type == 1)
                    {
                        GObjects[i].Position = Area.Element(1,1);
                        GObjects[i].Location = Area.Element(GObjects[i].Position.X, GObjects[i].Position.Y);
                        GObjects[i].Visible = false;
                        break;
                    }     
            }
        }

        public static void GenerateWall()
        {
        }

        public static void GenerateLevel()
        {

        }

        public static void GeneratePortal(Point[] list)
        {
            if (!portaladded)
            {
                Point pt = RandomPoint();

                for (int i = 0; list.Length > i; i++)
                {
                    if (pt == list[i])
                    {
                        pt = RandomPoint();
                        i = 0;
                    }
                    if (i == list.Length - 1)
                        for (int j = 0; GObjects.Count > j; j++)
                        {
                            if (pt == GObjects[j].Position)
                            {
                                pt = RandomPoint();
                                i = 0;
                                break;
                            }
                        }

                }

                Element Pb = new Element();
                Pb.Image = Image.FromFile("./Data/Портал.png");
                Pb.Position = pt;
                Pb.Location = Area.Element(Pb.Position.X, Pb.Position.Y);
                Pb.Size = new Size(26, 26);
                Pb.Type = 3; // Портал
                Pb.BackColor = System.Drawing.Color.Transparent;
                Pb.TabStop = false;
                Pb.Name = "E" + GObjects.Count;
                GObjects.Add(Pb);
                if (Click != null)
                    Click();
                portaladded = true;
            }



        }
    }
}
