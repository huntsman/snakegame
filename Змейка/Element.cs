﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Змейка
{
    class Element : PictureBox
    {
       public Point Position;
       public byte Type;
       // 0 - Змейка, 1 - еда, 2 - стена, 3 - портал

       public void ChangePosition(int Pstr, int Pst)
       {
           this.Position = new Point(this.Position.X + Pstr,this.Position.Y + Pst);
           this.Location = Area.Element(this.Position.X, this.Position.Y);
       }

       public void ClonePosition(int str, int st)
       {
           this.Position = new Point(str, st);
           this.Location = Area.Element(str, st);
       }

    }
}
