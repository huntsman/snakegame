﻿namespace Змейка
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.играToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяИграToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.счётToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выйтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оИгреToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.BeginB = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lScore = new System.Windows.Forms.Label();
            this.lBonus = new System.Windows.Forms.Label();
            this.lAllscore = new System.Windows.Forms.Label();
            this.LevelText = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.играToolStripMenuItem,
            this.оИгреToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(312, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // играToolStripMenuItem
            // 
            this.играToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяИграToolStripMenuItem,
            this.счётToolStripMenuItem,
            this.выйтиToolStripMenuItem});
            this.играToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Control;
            this.играToolStripMenuItem.Name = "играToolStripMenuItem";
            this.играToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.играToolStripMenuItem.Text = "Игра";
            // 
            // новаяИграToolStripMenuItem
            // 
            this.новаяИграToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.новаяИграToolStripMenuItem.Name = "новаяИграToolStripMenuItem";
            this.новаяИграToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.новаяИграToolStripMenuItem.Text = "Новая игра";
            this.новаяИграToolStripMenuItem.Click += new System.EventHandler(this.новаяИграToolStripMenuItem_Click);
            // 
            // счётToolStripMenuItem
            // 
            this.счётToolStripMenuItem.Name = "счётToolStripMenuItem";
            this.счётToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.счётToolStripMenuItem.Text = "Счёт";
            // 
            // выйтиToolStripMenuItem
            // 
            this.выйтиToolStripMenuItem.Name = "выйтиToolStripMenuItem";
            this.выйтиToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.выйтиToolStripMenuItem.Text = "Выйти";
            // 
            // оИгреToolStripMenuItem
            // 
            this.оИгреToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Control;
            this.оИгреToolStripMenuItem.Name = "оИгреToolStripMenuItem";
            this.оИгреToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.оИгреToolStripMenuItem.Text = "О игре";
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // BeginB
            // 
            this.BeginB.Location = new System.Drawing.Point(114, 167);
            this.BeginB.Name = "BeginB";
            this.BeginB.Size = new System.Drawing.Size(80, 23);
            this.BeginB.TabIndex = 2;
            this.BeginB.TabStop = false;
            this.BeginB.Text = "Начать";
            this.BeginB.UseVisualStyleBackColor = true;
            this.BeginB.Visible = false;
            this.BeginB.Click += new System.EventHandler(this.BeginB_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(131, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Итог:";
            this.label1.Visible = false;
            // 
            // lScore
            // 
            this.lScore.AutoSize = true;
            this.lScore.BackColor = System.Drawing.Color.Transparent;
            this.lScore.ForeColor = System.Drawing.SystemColors.Control;
            this.lScore.Location = new System.Drawing.Point(131, 84);
            this.lScore.Name = "lScore";
            this.lScore.Size = new System.Drawing.Size(35, 13);
            this.lScore.TabIndex = 4;
            this.lScore.Text = "label2";
            this.lScore.Visible = false;
            // 
            // lBonus
            // 
            this.lBonus.AutoSize = true;
            this.lBonus.BackColor = System.Drawing.Color.Transparent;
            this.lBonus.ForeColor = System.Drawing.SystemColors.Control;
            this.lBonus.Location = new System.Drawing.Point(131, 101);
            this.lBonus.Name = "lBonus";
            this.lBonus.Size = new System.Drawing.Size(35, 13);
            this.lBonus.TabIndex = 5;
            this.lBonus.Text = "label3";
            this.lBonus.Visible = false;
            // 
            // lAllscore
            // 
            this.lAllscore.AutoSize = true;
            this.lAllscore.BackColor = System.Drawing.Color.Transparent;
            this.lAllscore.ForeColor = System.Drawing.SystemColors.Control;
            this.lAllscore.Location = new System.Drawing.Point(131, 116);
            this.lAllscore.Name = "lAllscore";
            this.lAllscore.Size = new System.Drawing.Size(35, 13);
            this.lAllscore.TabIndex = 6;
            this.lAllscore.Text = "label3";
            this.lAllscore.Visible = false;
            // 
            // LevelText
            // 
            this.LevelText.AutoSize = true;
            this.LevelText.BackColor = System.Drawing.Color.Transparent;
            this.LevelText.ForeColor = System.Drawing.SystemColors.Control;
            this.LevelText.Location = new System.Drawing.Point(124, 151);
            this.LevelText.Name = "LevelText";
            this.LevelText.Size = new System.Drawing.Size(35, 13);
            this.LevelText.TabIndex = 7;
            this.LevelText.Text = "label2";
            this.LevelText.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(312, 336);
            this.Controls.Add(this.LevelText);
            this.Controls.Add(this.lAllscore);
            this.Controls.Add(this.lBonus);
            this.Controls.Add(this.lScore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BeginB);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Змейка";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem играToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаяИграToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem счётToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выйтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оИгреToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button BeginB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lScore;
        private System.Windows.Forms.Label lBonus;
        private System.Windows.Forms.Label lAllscore;
        private System.Windows.Forms.Label LevelText;
    }
}

